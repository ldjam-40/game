﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MoreMountains.CorgiEngine;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardHandler : MonoBehaviour {

    public static ScoreboardHandler Instance;

    public Text[] Texts;

    public CharacterUpdateStats[] ScoreBoard;

    void Awake() {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        List<Character> players = LevelManager.Instance.Players;
        ScoreBoard = new CharacterUpdateStats[players.Count];
        for (int i = 0; i < ScoreBoard.Length; i++) {
            ScoreBoard[i] = players[i].GetComponent<CharacterUpdateStats>();
        }
	}
	
	// Update is called once per frame
	void Update () {
        List<Character> players = LevelManager.Instance.Players;
		for (int i = 0; i < players.Count; i++) {
            CharacterUpdateStats stats = ScoreBoard[i];
			int score = stats.killCount - stats.deathCount;
			if (score < 0) {
				score = 0;
			}

            Texts[i].text = "K: " + stats.killCount.ToString("00") + " D: " + stats.deathCount.ToString("00");
        }
    }
}

