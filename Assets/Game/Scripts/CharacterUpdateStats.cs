﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine // you might want to use your own namespace here
{
	/// <summary>
	/// TODO_DESCRIPTION
	/// </summary>
	[AddComponentMenu("Corgi Engine/Character/Abilities/CharacterUpdateStats")]
	public class CharacterUpdateStats : CharacterAbility //, MMEventListener<CorgiEnginePointsEvent>
	{
		/// This method is only used to display a helpbox text
		/// at the beginning of the ability's inspector
		public override string HelpBoxText() { return "Character Update stats is a component that handles the characters kill and death count and edits his abilities (movement speed and size) based on these stats"; }

		[Header("Update Parameters")]
		/// declare your parameters here
		public float maxMultiplier = 3;
		public float sizeFactor = 0.75f;
		public float movementFactor = 2;
		public float jumpFactor = 2;
		public int score = 0;
        public int deathCount;
        public int killCount;

        /// declaration of internal parameters
        public Vector3 initialSize {get; protected set;}
		private CharacterHorizontalMovement mover;
		private CharacterJump jumper;
		private float initialWalkSpeed;
		private float initialJumpHeight;
		private string CharacterId;

		/// <summary>
		/// Here you should initialize our parameters
		/// </summary>
		protected override void Initialization()
		{
			base.Initialization();
			initialSize = transform.localScale;
			deathCount = 0;
			killCount = 0;
			mover = GetComponent<CharacterHorizontalMovement> ();
			jumper = GetComponent<CharacterJump> ();
			initialWalkSpeed = mover.WalkSpeed;
			initialJumpHeight = jumper.JumpHeight;
			CharacterId = GetComponent<Character> ().PlayerID;
		}

		///<summary>
		/// Getters and Setters
		/// </summary>
		public virtual int getDeathCount() 
		{
			return this.deathCount;

		}

		/// <summary>
		/// Updates the Player Size, Speed and jump Height based on Kill/Death difference
		/// </summary>
		public virtual void changeBehaviour() 
		{
			score = killCount - deathCount;
			if (score > 0) { //Player has more kills than deaths and is winning
				float m_factor = 1 / (movementFactor * score);
				float newSpeed = initialWalkSpeed * m_factor; //New speed is calculated based on the initial speed
				mover.WalkSpeed = newSpeed;
				mover.MovementSpeed = newSpeed;
				Debug.Log (mover.WalkSpeed);

				float j_factor = 1 / (jumpFactor * score);
				float newHeight = initialJumpHeight * j_factor; //New jump height is calculated based on the initial height
				jumper.JumpHeight = newHeight;

				float s_factor = sizeFactor * score;
				if (1 + s_factor <= maxMultiplier) {
					Vector3 newSize = initialSize * (1 + s_factor); //New size is calculated based on the initial size
					transform.localScale = newSize;
				}

			} else { //Player is even or losing
				mover.WalkSpeed = initialWalkSpeed;
				jumper.JumpHeight = initialJumpHeight;
				transform.localScale = initialSize;
			}

		}


		/// <summary>
		/// Called when the player dies, changes size and movement behaviour
		/// </summary>
		public virtual void onDeath()
		{
			this.deathCount++;
			changeBehaviour ();
		}

		/// <summary>
		/// Called when the player killed another player, changes size and movement behaviour.
		/// </summary>
		public virtual void onKill()
		{
			this.killCount++;
			changeBehaviour ();
		}


		/// <summary>
		/// Every frame, we check if we're crouched and if we still should be
		/// </summary>
		public override void ProcessAbility()
		{
			base.ProcessAbility();
		}

		/// <summary>
		/// Called at the start of the ability's cycle, this is where you'll check for input
		/// </summary>
		protected override void HandleInput()
		{			

		}

		/// <summary>
		/// If we're pressing down, we check for a few conditions to see if we can perform our action
		/// </summary>
		protected virtual void DoSomething()
		{
			// if the ability is not permitted
			if ( !AbilityPermitted
				// or if we're not in our normal stance
				|| (_condition.CurrentState != CharacterStates.CharacterConditions.Normal)
				// or if we're grounded
				|| (!_controller.State.IsGrounded)
				// or if we're gripping
				|| (_movement.CurrentState == CharacterStates.MovementStates.Gripping) )
			{
				// we do nothing and exit
				return;
			}

			// if we're still here, we display a text log in the console
			MMDebug.DebugLogTime("We're doing something yay!");
		}

		/// <summary>
		/// Adds required animator parameters to the animator parameters list if they exist
		/// </summary>
		protected override void InitializeAnimatorParameters()
		{
			RegisterAnimatorParameter ("TODO_ANIMATOR_PARAMETER_NAME", AnimatorControllerParameterType.Bool);
		}

		/// <summary>
		/// At the end of the ability's cycle,
		/// we send our current crouching and crawling states to the animator
		/// </summary>
		public override void UpdateAnimator()
		{
			MMAnimator.UpdateAnimatorBool(_animator,"TODO_ANIMATOR_PARAMETER_NAME",(_movement.CurrentState == CharacterStates.MovementStates.Crouching), _character._animatorParameters);			
		}
	}
}


