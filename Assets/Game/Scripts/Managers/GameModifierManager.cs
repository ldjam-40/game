﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Game/Managers/Game Event (Modifier) Manager")]
public class GameModifierManager : Singleton<GameModifierManager>, MMEventListener<GameModifierManager.ModifierEvent> {

    public bool RandomModifiers = true;

    public float TimeBetweenMin = 60;
    public float TimeBetweenMax = 120;
    public float DurationMin = 30;
    public float DurationMax = 60;

    public ModifierEvent ModNext;
    public ModifierEvent ModCurrent;

    public float RemainingTime = 0;

	void Start() {
        StartCoroutine(ModifierLoop());

        MMEventManager.AddListener(this);
	}

    private IEnumerator ModifierLoop() {
        // Unity overriddes == null; wir können damit überprüfen, ob
        // wir noch existieren.
        while (this != null) {
            yield return new WaitUntil(() => RandomModifiers);

            // ModCurrent.IsActive == false - reset previous mod in all listeners.
            ModCurrent.Enabled = false;
            MMEventManager.TriggerEvent(ModCurrent);

            ModCurrent = default(ModifierEvent);
            float wait = Random.Range(TimeBetweenMin, TimeBetweenMax);
            ModNext = new ModifierEvent() {
                Mod = (Modifier) Random.Range(1, (int) Modifier.Count),
                TimeStart = Time.unscaledTime + wait,
                Duration = Random.Range(DurationMin, DurationMax)
            };
            yield return new WaitForSeconds(wait);

            ModCurrent = ModNext;
            ModNext = default(ModifierEvent);
            ModCurrent.Enabled = true;
            MMEventManager.TriggerEvent(ModCurrent);
            yield return new WaitForSeconds(ModCurrent.Duration);
        }
    }

    void Update() {
		
	}

    void LateUpdate() {
        RemainingTime = (ModCurrent.Mod != Modifier.None ? ModCurrent.TimeEnd : ModNext.TimeStart) - Time.time;
    }

    public void OnMMEvent(ModifierEvent mod) {
        // Damit wir auch manuell getriggerte Modifier handlen können.
        bool enabled = mod.Enabled;
        switch (mod.Mod) {
            case Modifier.Moon:
                // Handled in CharacterGameModifierListener
                break;

            case Modifier.Fast:
                if (enabled)
                    GameManager.Instance.SetTimeScale(2f);
                else
                    GameManager.Instance.ResetTimeScale();
                break;
            case Modifier.Slow:
                if (enabled)
                    GameManager.Instance.SetTimeScale(0.5f);
                else
                    GameManager.Instance.ResetTimeScale();
                break;
        }
    }

    /// <summary>
    /// Modifier enum. Namen werden auch im Text verwendet, '_' wird mit ' ' ersetzt.
    /// </summary>
    public enum Modifier {
        None,
        Moon,
        Fast,
        Slow,
        Count
    }

    public struct ModifierEvent {
        public Modifier Mod;
        public float TimeStart;
        public float Duration;
        public float TimeEnd { get { return TimeStart + Duration; } }
        public bool Enabled;
    }

}
