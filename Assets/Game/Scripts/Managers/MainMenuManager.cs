﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {

    public RectTransform Title;
    private float TitleStart = 0;

    public CanvasGroup MainPanel;

    private Stack<CanvasGroup> PanelStack = new Stack<CanvasGroup>();

    private CanvasGroup CurrentPanel;
    private bool PanelSliding;

    void Start() {
        TitleStart = Title.anchoredPosition.y;

        CurrentPanel = MainPanel;
    }

    void Update() {
        Title.anchoredPosition = new Vector2(Title.anchoredPosition.x, Mathf.Sin(Mathf.PI * Time.time) * 10 + TitleStart);
        Title.localRotation = Quaternion.Euler(0f, 0f, Mathf.Sin(Mathf.PI * Time.time * 1.3f) * 5);
        float scale = 0.6f + 0.1f * Mathf.Sin(Mathf.PI * Time.time * 1.1f);
        Title.localScale = new Vector3(scale, scale, 1f);
        Cursor.visible = true;
    }

    public void OnPlay() {
        // LoadingSceneManager.LoadScene("Game/Scenes/Arena_" + GameSettingsManager.Scene);
        SceneManager.LoadScene("Game/Scenes/Arena ++");
    }

    public void OnQuit() {
        Application.Quit();
    }

    public void PanelPush(CanvasGroup target) {
        PanelStack.Push(CurrentPanel);
        StartCoroutine(PanelSlide(target, 1));
    }

    public void PanelPop() {
        if (PanelStack.Count == 0) {
            PanelSlide(MainPanel, -1);
            return;
        }
        StartCoroutine(PanelSlide(PanelStack.Pop(), -1));
    }

    public IEnumerator PanelSlide(CanvasGroup target, int dir = 1) {
        if (target == CurrentPanel) yield break;
        if (PanelSliding) yield break;
        PanelSliding = true;

        target.interactable = false;
        target.blocksRaycasts = false;
        CurrentPanel.interactable = false;
        CurrentPanel.blocksRaycasts = false;

        RectTransform targetTrans = target.GetComponent<RectTransform>();
        RectTransform currentTrans = CurrentPanel.GetComponent<RectTransform>();

        const float dur = 0.3f;
        for (float start = Time.time, end = start + dur, t = 0; t < 1; t = (Time.time - start) / dur) {
            float st = t;
            st = 1f - Mathf.Pow(1f - Mathf.Sin(Mathf.PI * st * 0.5f), 2);

            target.alpha = st;
            CurrentPanel.alpha = 1f - st;

            targetTrans.anchoredPosition = new Vector2(Mathf.Lerp(dir * 400f, 0f, st), 0f);
            currentTrans.anchoredPosition = new Vector2(Mathf.Lerp(0f, dir * -400f, st), 0f);

            yield return null;
        }

        target.alpha = 1f;
        CurrentPanel.alpha = 0f;

        targetTrans.anchoredPosition = new Vector2(0f, 0f);

        target.interactable = true;
        target.blocksRaycasts = true;
        CurrentPanel.interactable = false;
        CurrentPanel.blocksRaycasts = false;

        CurrentPanel = target;

        PanelSliding = false;
    }

}
