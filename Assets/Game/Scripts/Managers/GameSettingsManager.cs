﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSettingsManager : Singleton<GameSettingsManager> {

    public static int PlayerCount = 4;
    public static int Time = 60 * 3;
    public static float ItemRate = 1f;
    public static int Scene = 0;

    public Text PlayerCountLabel;
    public Slider PlayerCountSlider;
    public Text TimeLabel;
    public Slider TimeSlider;
    public Text ItemRateLabel;
    public Slider ItemRateSlider;
    public Dropdown SceneDropdown;

    void Start() {
        PlayerCount = PlayerPrefs.GetInt("GameSetting_PlayerCount", PlayerCount);
        Time = PlayerPrefs.GetInt("GameSetting_Time", Time);
        ItemRate = PlayerPrefs.GetFloat("GameSetting_ItemRate", ItemRate);
        Scene = PlayerPrefs.GetInt("GameSetting_Scene", Scene);
        StartCoroutine(DelayedUpdateUI(null));

        DontDestroyOnLoad(this);
        SceneManager.activeSceneChanged += (prev, current) => StartCoroutine(DelayedUpdateUI(new WaitUntil(() => current.isLoaded)));
    }

    public IEnumerator DelayedUpdateUI(IEnumerator delay) {
        yield return delay;
        UpdateUI();
    }

    public void UpdateUI() {
        if (Find(ref PlayerCountLabel, "PlayerCountLabel") != null)
            PlayerCountLabel.text = PlayerCount + " slime" + (PlayerCount == 1 ? "" : "s");
        if (Find(ref PlayerCountSlider, "PlayerCountSlider") != null)
            PlayerCountSlider.value = PlayerCount;

        if (Find(ref TimeLabel, "TimeLabel") != null)
            TimeLabel.text = (Time/60) + " min" + ((Time/60) == 1 ? "" : "s");
        if (Find(ref TimeSlider, "TimeSlider") != null)
            TimeSlider.value = Time / 60;

        if (Find(ref ItemRateLabel, "ItemRateLabel") != null)
            ItemRateLabel.text = Mathf.RoundToInt(ItemRate) + "x drops";
        if (Find(ref ItemRateSlider, "ItemRateSlider") != null)
            ItemRateSlider.value = ItemRate;

        if (Find(ref SceneDropdown, "SceneDropdown") != null) {
            SceneDropdown.value = Scene;
            SceneDropdown.RefreshShownValue();
        }
    }

    private T Find<T>(ref T value, string name) where T : Component {
        if (value != null)
            return value;
        GameObject go = GameObject.Find(name);
        if (go == null)
            return value = null;
        return value = go.GetComponent<T>();
    }

    public void OnChangePlayerCount(float value) {
        PlayerCount = Mathf.RoundToInt(value);
        PlayerPrefs.SetInt("GameSetting_PlayerCount", PlayerCount);
        UpdateUI();
    }

    public void OnChangeTime(float value) {
        Time = 60 * Mathf.RoundToInt(value);
        PlayerPrefs.SetInt("GameSetting_Time", Time);
        UpdateUI();
    }

    public void OnChangeItemRate(float value) {
        ItemRate = value;
        PlayerPrefs.SetFloat("GameSetting_ItemRate", ItemRate);
        UpdateUI();
    }

    public void OnChangeScene(int value) {
        if (Scene == value)
            return;
        Scene = value;
        PlayerPrefs.SetInt("GameSetting_Scene", Scene);
        UpdateUI();
    }

}
