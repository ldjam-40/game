﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreenShakeManager : MonoBehaviour {

    public static ScreenShakeManager Instance;

    public RectTransform[] HUDs;

    void Awake() {
        Instance = this;
    }

    public void Trigger(int killedID, int killedByID) {
        StartCoroutine(Shake(killedID, killedByID));
    }

    IEnumerator Shake(int killedID, int killedByID) {
        float time = Time.timeScale;
        GameManager.Instance.SetTimeScale(time);
        Camera cam = Camera.main;

        RectTransform killedHUD = HUDs[killedID - 1];
        Vector2 killedHUDPos = killedHUD.anchoredPosition;
        RectTransform killedByHUD = HUDs[killedByID - 1];
        Vector2 killedByHUDPos = killedByHUD.anchoredPosition;

        const float dur = 0.4f;
        Vector3 camShakePrev = Vector3.zero;
        for (float start = Time.unscaledTime, end = start + dur, t = 0; t < 1; t = (Time.unscaledTime - start) / dur) {
            float st = t;
            st = 1f - Mathf.Pow(1f - Mathf.Sin(Mathf.PI * st * 0.5f), 2);

            float max = (1f - st) * 2f;

            cam.transform.Translate(-camShakePrev);
            cam.transform.Translate(camShakePrev = new Vector3(
                3f * (Random.Range(0f, max) - (max) * 0.5f),
                3f * (Random.Range(0f, max) - (max) * 0.5f),
                0f
            ));

            killedHUD.anchoredPosition = killedHUDPos + new Vector2(
                32f * (Random.Range(0f, max) - (max) * 0.5f),
                32f * (Random.Range(0f, max) - (max) * 0.5f)
            );
            killedByHUD.anchoredPosition = killedByHUDPos + new Vector2(
                32f * (Random.Range(0f, max) - (max) * 0.5f),
                32f * (Random.Range(0f, max) - (max) * 0.5f)
            );

            GameManager.Instance.ResetTimeScale();
            GameManager.Instance.SetTimeScale(Mathf.Lerp(time * 0.3f, time, t));
            yield return null;
        }

        killedHUD.anchoredPosition = killedHUDPos;
        killedByHUD.anchoredPosition = killedByHUDPos;

        GameManager.Instance.ResetTimeScale();
    }

}
