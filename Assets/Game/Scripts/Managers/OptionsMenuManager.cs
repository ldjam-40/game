﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuManager : MonoBehaviour {

    void Start() {
        SoundManager.Instance.RuntimeMusicVolume = PlayerPrefs.GetFloat("MusicVolume", 0.3f);
        SoundManager.Instance.SfxVolume = PlayerPrefs.GetFloat("SfxVolume", 0.9f);

        Slider s;
        if ((s = Find<Slider>("MusicVolumeSlider")) != null)
            s.value = SoundManager.Instance.MusicVolume;
        if ((s = Find<Slider>("SoundsVolumeSlider")) != null)
            s.value = SoundManager.Instance.SfxVolume;
    }

    private T Find<T>(string name) where T : Component {
        GameObject go = GameObject.Find(name);
        if (go == null)
            return null;
        return go.GetComponent<T>();
    }

    public void OnChangeMusicVolume(float value) {
        SoundManager.Instance.RuntimeMusicVolume = value;
        PlayerPrefs.SetFloat("MusicVolume", SoundManager.Instance.MusicVolume);
    }

    public void OnChangeSoundsVolume(float value) {
        SoundManager.Instance.SfxVolume = value;
        PlayerPrefs.SetFloat("SfxVolume", SoundManager.Instance.SfxVolume);
    }

}
