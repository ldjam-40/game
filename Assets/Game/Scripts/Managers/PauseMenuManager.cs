﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour {

    public RectTransform Title;
    private float TitleStart = 0;

    void Awake() {
        TitleStart = Title.anchoredPosition.y;
    }

    void Update() {
        Title.anchoredPosition = new Vector2(Title.anchoredPosition.x, Mathf.Sin(Mathf.PI * Time.unscaledTime) * 5 + TitleStart);
        Title.localRotation = Quaternion.Euler(0f, 0f, Mathf.Sin(Mathf.PI * Time.unscaledTime * 1.3f) * 3);
        float scale = 0.4f + 0.05f * Mathf.Sin(Mathf.PI * Time.unscaledTime * 1.1f);
        Title.localScale = new Vector3(scale, scale, 1f);
    }

    public void OnRestart() {
        GameManager.Instance.ResetTimeScale();
        GameManager.Instance.SetTimeScale(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnLoadScene(string id) {
        GameManager.Instance.ResetTimeScale();
        GameManager.Instance.SetTimeScale(1f);
        SceneManager.LoadScene(id);
    }

}
