﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using UnityEngine.SceneManagement;
using System.Linq;

[AddComponentMenu("Game/Managers/Game Level Manager")]
public class GameLevelManager : MultiplayerLevelManager {

	/// Resapwn settings 
	[Space(10)]
	[Header("Respawn Settings")]
	[Information("You can set, how many times a player can respawn and how long a respawn takes after the player died.", InformationAttribute.InformationType.Info, false)]
	public float DeathRespawnDelay = 5.0f;

	/// Round settings
	[Space(10)]
	[Header("Round Settings")]
	[Information("Configure the round settings like max rounds and round duration.", InformationAttribute.InformationType.Info, false)]
	public bool SingleAliveWins = false;
	public int RoundCount = 1;
	public float RemainingRoundTime;

	public override void Start()
	{
		base.Start ();
		InitializeRoundTimer ();
	}
	protected virtual void InitializeRoundTimer() {
        RemainingRoundTime = GameSettingsManager.Time;
	}

	public virtual void LateUpdate(){
		RemainingRoundTime -= Time.deltaTime;
		CheckMultiplayerEndGame ();
	}

	/// <summary>
	/// Kills the specified player , and triggers it's respawn
	/// </summary>
	public override void KillPlayer(Character player)
	{
		Health characterHealth = player.GetComponent<Health>();
		if (characterHealth == null) {
			return;
		} else {
			// we kill the character
			characterHealth.Kill ();
			player.gameObject.SetActive(false);
			StartCoroutine (RespawnPlayer (player));
		}

		CheckMultiplayerEndGame ();
	}

	protected override void CheckMultiplayerEndGame() {
		if (SingleAliveWins) {
			base.CheckMultiplayerEndGame ();
		}

		if (RemainingRoundTime <= 0f) {
            if (!DeterminingWinner)
			    DetermineWinner ();
		}
	}

    protected bool DeterminingWinner = false;
	protected virtual void DetermineWinner() {
        DeterminingWinner = true;

        StartCoroutine(MultiplayerEndGame (""));
	}

    protected override IEnumerator MultiplayerEndGame(string winnerID) {
        // we wait for 1 second
        yield return new WaitForSeconds(1f);

        GameManager.Instance.SetTimeScale(1f);
        const float dur = 3f;
        for (float start = Time.unscaledTime, end = start + dur, t = 0; t < 1; t = (Time.unscaledTime - start) / dur) {
            float st = t;
            st = 1f - Mathf.Pow(1f - Mathf.Sin(Mathf.PI * st * 0.5f), 2);

            GameManager.Instance.ResetTimeScale();
            GameManager.Instance.SetTimeScale(1f - st);
            yield return null;
        }
        GameManager.Instance.ResetTimeScale();
        GameManager.Instance.SetTimeScale(1f);

        // we freeze all characters
        FreezeCharacters();
        yield return new WaitForSeconds(0.2f);

        // if we find a MPGUIManager, we display the end game screen with the name of the winner
        if (GUIManager.Instance.GetComponent<MultiplayerGUIManager>() != null) {
            GUIManager.Instance.GetComponent<MultiplayerGUIManager>().ShowMultiplayerEndgame();

            int count = 0;
            string winners = "";

            IOrderedEnumerable<CharacterUpdateStats> board = ScoreboardHandler.Instance.ScoreBoard.OrderByDescending(s => s.score);
            int score = board.First().score;
            foreach (CharacterUpdateStats s in board) {
                if (s.score < score)
                    break;
                if (count > 0)
                    winners += "&";
                winners += s.GetComponent<Character>().PlayerID.ToLowerInvariant().Replace("player", "").Trim();
                count++;
            }

            GUIManager.Instance.GetComponent<MultiplayerGUIManager>().SetMultiplayerEndgameText("PLAYER" + (count == 1 ? " " : "S ") + winners + " WIN" + (count == 1 ? "S" : ""));
        }
        // we wait for a few seconds
        yield return new WaitForSeconds(5f);
        // we reload the current scene to start a new game
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Removes the specified player from the game.
    /// </summary>
    /// <returns>The player.</returns>
    /// <param name="player">Player.</param>
    protected virtual IEnumerator RespawnPlayer(Character player)
	{
		yield return new WaitForSeconds (DeathRespawnDelay);
		player.gameObject.SetActive(true);
		SpawnSingleCharacter (Players.IndexOf(player));
	}

	/// <summary>
	/// Spawns a playable character into the scene
	/// </summary>
	protected virtual void SpawnSingleCharacter(int index)
	{
		// in debug mode we spawn the player on the debug spawn point
		#if UNITY_EDITOR 
		if (DebugSpawn!= null)
		{
			DebugSpawn.SpawnPlayer(Players[index]);
		}
		else if (Checkpoints[index] != null)
		{
			Checkpoints[index].SpawnPlayer(Players[index]);
		}
		#else
		if (Checkpoints[index] != null)
		{			
			Checkpoints[index].SpawnPlayer(Players[0]);
		}
		#endif	
	}

    protected override void InstantiatePlayableCharacters() {
        Players = new List<Character>();

        if (PlayerPrefabs == null || PlayerPrefabs.Length == 0)
            return;

        for (int i = 0; i < PlayerPrefabs.Length && i < GameSettingsManager.PlayerCount; i++) {
            Character playerPrefab = PlayerPrefabs[i];
            Character newPlayer = Instantiate(playerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            newPlayer.name = playerPrefab.name;
            Players.Add(newPlayer);

            if (playerPrefab.CharacterType != Character.CharacterTypes.Player) {
                Debug.LogWarning("LevelManager : The Character you've set in the LevelManager isn't a Player, which means it's probably not going to move. You can change that in the Character component of your prefab.");
            }
        }
    }

}
