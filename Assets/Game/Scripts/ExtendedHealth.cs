﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class ExtendedHealth : Health, MMEventListener<MMDamageTakenEvent> {

	private Animator animator;

	[Header("Death settings")]
	public string DeathAnimation = "Die";
	public AudioClip DeathAudioClip;

	protected virtual void Awake() {
		animator = GetComponent<Animator> ();
	}

	protected override void Start() {
		base.Start ();

		MMEventManager.AddListener(this);
	}

	protected virtual void OnDestroy(){
		MMEventManager.RemoveListener(this);
	}

	/// <summary>
	/// Plays a sound when the character is hit
	/// </summary>
	protected override void PlayHitSfx()
	{
		if (CurrentHealth > 0 && DamageSfx!=null)
		{
			SoundManager.Instance.PlaySound(DamageSfx,transform.position);
		}
	}

	#region MMEventListener implementation
	public void OnMMEvent (MMDamageTakenEvent eventType)
	{
		// This character
		if (eventType.AffectedCharacter != null && 
			eventType.AffectedCharacter.gameObject == gameObject) {
			if (eventType.CurrentHealth <= 0) {
				if (DeathAnimation != null) {
					animator.SetTrigger (DeathAnimation);
				}

				if (DeathAudioClip != null) {
					SoundManager.Instance.PlaySound (DeathAudioClip, transform.position, false);
				}
			}
		}
	}
	#endregion
}
