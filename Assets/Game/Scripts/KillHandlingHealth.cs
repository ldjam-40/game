﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{

	public class KillHandlingHealth : Health
	{

		/// <summary>
		/// Kills the character, vibrates the device, UPDATES PLAYER STATS, instantiates death effects, handles points, etc
		/// </summary>
		public override void Kill ()
		{
			// Update the Stats for this player
			Debug.Log("Entered KillHandlingHealth");
			CharacterUpdateStats stats = this.GetComponent(typeof (CharacterUpdateStats)) as CharacterUpdateStats;
			stats.onDeath();

			// we make our handheld device vibrate
			if (VibrateOnDeath) {
				#if UNITY_ANDROID || UNITY_IPHONE
			Handheld.Vibrate();	
				#endif
			}

			// we prevent further damage
			DamageDisabled ();

			// instantiates the destroy effect
			if (DeathEffect != null) {
				GameObject instantiatedEffect = (GameObject)Instantiate (DeathEffect, transform.position, transform.rotation);
				instantiatedEffect.transform.localScale = transform.localScale;
			}

			// Adds points if needed.
			if (PointsWhenDestroyed != 0) {
				// we send a new points event for the GameManager to catch (and other classes that may listen to it too)
				MMEventManager.TriggerEvent (new CorgiEnginePointsEvent (PointsMethods.Add, PointsWhenDestroyed));
			}

			// if we have a controller, removes collisions, restores parameters for a potential respawn, and applies a death force
			if (_controller != null) {
				// we make it ignore the collisions from now on
				if (CollisionsOffOnDeath) {
					_controller.CollisionsOff ();	
					if (_collider2D != null) {
						_collider2D.enabled = false;
					}
				}

				// we reset our parameters
				_controller.ResetParameters ();

				// we apply our death force
				if (DeathForce != Vector2.zero) {
					_controller.GravityActive (true);
					_controller.SetForce (DeathForce);		
				}
			}

			if (OnDeath != null) {
				OnDeath ();
			}

			// if we have a character, we want to change its state
			if (_character != null) {
				// we set its dead state to true
				_character.ConditionState.ChangeState (CharacterStates.CharacterConditions.Dead);
				_character.Reset ();

				// if this is a player, we quit here
				if (_character.CharacterType == Character.CharacterTypes.Player) {
					return;
				}
			}

			if (DelayBeforeDestruction > 0f) {
				Invoke ("DestroyObject", DelayBeforeDestruction);
			} else {
				// finally we destroy the object
				DestroyObject ();	
			}


		}


		/// <summary>
		/// Revive this object.
		/// </summary>
		public override void Revive()
		{
			if (!_initialized)
			{
				return;
			}

			if (_collider2D != null)
			{
				_collider2D.enabled = true;
			}
			if (_controller != null)
			{
				_controller.CollisionsOn();
				_controller.SetForce(Vector2.zero);
				_controller.ResetParameters();
			}
			if (_character != null)
			{
				_character.ConditionState.ChangeState(CharacterStates.CharacterConditions.Normal);
			}

			if (RespawnAtInitialLocation)
			{
				transform.position = _initialPosition;
			}

			Initialization();
			ResetSpriteColor ();
			UpdateHealthBar(false);
			if (OnRevive != null)
			{
				OnRevive ();
			}
		}


	}
}