﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

[AddComponentMenu("Game/Items/Item Spawner")]
public class ItemSpawner : MonoBehaviour {

	private bool CanSpawnItem = false;
	private bool CanSpawnSpecialItem = false;

	/// Item spawn settings 
	[Header("Item Respawn Settings")]
	[Information("Configure the item respawn rate in seconds.", InformationAttribute.InformationType.Info, false)]
	[Range(0f, 1000f)]
	public float MinSpawnDelay = 30f;
	[Range(0f, 1000f)]
	public float MaxSpawnDelay = 60f;
	public ItemSpawnPoint[] ItemSpawnPoints;
	public GameObject[] Items;
	public GameObject ItemsParent;

	/// Special item spawn settings 
	[Header("Special Item Respawn Settings")]
	[Information("Configure the special item respawn rate in seconds.", InformationAttribute.InformationType.Info, false)]
	[Range(0f, 1000f)]
	public float SpecialMinSpawnDelay = 60f;
	[Range(0f, 1000f)]
	public float SpecialMaxSpawnDelay = 120f;
	public ItemSpawnPoint[] SpecialItemSpawnPoints;
	public GameObject[] SpecialItems;
	public GameObject SpecialItemsParent;

	protected virtual void OnValidate() {
		MaxSpawnDelay = Mathf.Clamp (MaxSpawnDelay, MinSpawnDelay, 1000f);
		MinSpawnDelay = Mathf.Clamp (MinSpawnDelay, 0f, MaxSpawnDelay);

		SpecialMaxSpawnDelay = Mathf.Clamp (SpecialMaxSpawnDelay, SpecialMinSpawnDelay, 1000f);
		SpecialMinSpawnDelay = Mathf.Clamp (SpecialMinSpawnDelay, 0f, SpecialMaxSpawnDelay);
	}

	protected virtual void Start() {
		StartCoroutine (TriggerSpawnTimer ());
		StartCoroutine (TriggerSpecialSpawnTimer ());

	}

	protected virtual void Update() {
		if (CanSpawnItem && GameSettingsManager.ItemRate > 0) {
			StartCoroutine (TriggerSpawnTimer ());
		}

		if(CanSpawnSpecialItem && GameSettingsManager.ItemRate > 0) {
			StartCoroutine (TriggerSpecialSpawnTimer ());
		}
	}

	IEnumerator TriggerSpawnTimer() {
		CanSpawnItem = false;
		var Delay = Random.Range (MinSpawnDelay, MaxSpawnDelay) / GameSettingsManager.ItemRate;
		yield return new WaitForSeconds (Delay);

		// Spawn Item here
		StartCoroutine(SpawnItem());

		CanSpawnItem = true;
		yield return null;
	}

	IEnumerator SpawnItem() {
		if (Items.Length != 0 && ItemSpawnPoints.Length != 0) {
			var ItemIndex = Random.Range (0, Items.Length);
			var SpawnIndex = Random.Range (0, ItemSpawnPoints.Length);
			var Item = ItemSpawnPoints [SpawnIndex].SpawnItem (Items [ItemIndex]);

			if (Item != null && ItemsParent != null) {
				Item.transform.parent = ItemsParent.transform;
			}
		} else {
			Debug.LogError ("No items or spawns found!", this);
		} 
		yield return null;
	}

	IEnumerator TriggerSpecialSpawnTimer() {
		CanSpawnSpecialItem = false;
		var Delay = Random.Range (SpecialMinSpawnDelay, SpecialMaxSpawnDelay) / GameSettingsManager.ItemRate;
		yield return new WaitForSeconds (Delay);

		// Spawn Item here
		StartCoroutine(SpawnSpecialItem());

		CanSpawnSpecialItem = true;
		yield return null;
	}

	IEnumerator SpawnSpecialItem() {
		if (SpecialItems.Length != 0 && SpecialItemSpawnPoints.Length != 0) {
			var ItemIndex = Random.Range (0, SpecialItems.Length);
			var SpawnIndex = Random.Range (0, SpecialItemSpawnPoints.Length);
			var Item = SpecialItemSpawnPoints [SpawnIndex].SpawnItem (SpecialItems [ItemIndex]);

			if (Item != null && ItemsParent != null) {
				Item.transform.parent = ItemsParent.transform;
			}
		} else {
			Debug.LogError ("No special items or spawns found!", this);
		} 
		yield return null;
	}
}