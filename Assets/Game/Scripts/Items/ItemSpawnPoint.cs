﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class ItemSpawnPoint : MonoBehaviour {

	private GameObject SpawnedItem;

	/// <summary>
	/// Spawns an GameObject at the SpawnPoint's position
	/// </summary>
	/// <returns>The spawned item. Is NULL if the Item was not spawned.</returns>
	/// <param name="item">Item the prefab item to be spawned.</param>
	public GameObject SpawnItem(GameObject item){
		if (SpawnedItem != null && SpawnedItem.gameObject.activeSelf) {
			Destroy (SpawnedItem);
		}
	
		SpawnedItem = Instantiate (item, transform.position, Quaternion.identity).gameObject;
		return SpawnedItem;
	}
}
