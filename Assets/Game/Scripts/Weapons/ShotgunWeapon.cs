﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Game/Weapons/Shotgun Weapon")]
public class ShotgunWeapon : ProjectileWeapon {

    public int Count = 5;
    public float Alpha = 15;
    public float Inaccuracy = 2;

    protected override void WeaponUse() {
        // base.WeaponUse();

        SetParticleEffects(true);
        SfxPlayWeaponUsedSound();

        DetermineSpawnPosition();

        for (float i = 0; i < Count; i++) {
            float angle = (Mathf.Sin(Mathf.PI * (i / Count)) - 0.5f) * Alpha + Random.value * Inaccuracy;
            SpawnProjectile(_spawnPosition, angle);
        }
    }

    public virtual GameObject SpawnProjectile(Vector3 spawnPosition, float angle, bool triggerObjectActivation = true) {
        /// we get the next object in the pool and make sure it's not null
        GameObject nextGameObject = ObjectPooler.GetPooledGameObject();

        // mandatory checks
        if (nextGameObject == null) { return null; }
        if (nextGameObject.GetComponent<PoolableObject>() == null) {
            throw new System.Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
        }
        // we position the object
        nextGameObject.transform.position = spawnPosition;
        // we set its direction

        Projectile projectile = nextGameObject.GetComponent<Projectile>();
        if (projectile != null) {
            projectile.SetWeapon(this);
            projectile.SetOwner(Owner.gameObject);
        }
        // we activate the object
        nextGameObject.gameObject.SetActive(true);

        transform.Rotate(new Vector3(0f, 0f, angle));


        if (projectile != null) {
            projectile.SetDirection(transform.right * (Flipped ? -1 : 1), transform.rotation, Owner.IsFacingRight);
        }

        if (triggerObjectActivation) {
            if (nextGameObject.GetComponent<PoolableObject>() != null) {
                nextGameObject.GetComponent<PoolableObject>().TriggerOnSpawnComplete();
            }
        }

        return (nextGameObject);
    }

}
