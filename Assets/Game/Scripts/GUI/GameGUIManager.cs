﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GameGUIManager : MultiplayerGUIManager {

	private GameLevelManager LevelManager;
    private GameModifierManager ModifierManager;

    /// Round timer setitngs 
    [Space(10)]
	[Header("Rounter Timer")]
	[Information("Set the round timer GUI elements.", InformationAttribute.InformationType.Info, false)]
	/// the points counter
	public Text TimerText;

    /// Modifier events timer
    [Space(10)]
    [Header("Modifier Timer")]
    [Information("Set the modifier timer GUI elements.", InformationAttribute.InformationType.Info, false)]
    public Text ModifierText;

    protected override void Start() {
		base.Start ();
		LevelManager = GameLevelManager.Instance as GameLevelManager;
		ModifierManager = GameModifierManager.Instance;
	}

	protected virtual void LateUpdate() {
		TimerText.text = Mathf.Clamp(LevelManager.RemainingRoundTime, 0, int.MaxValue).ToString("000");

        if (ModifierManager.ModCurrent.Mod != GameModifierManager.Modifier.None) {
            ModifierText.text = ModifierManager.ModCurrent.Mod.ToString().Replace('_', ' ') + " ends\nin ";
        } else {
            ModifierText.text = ModifierManager.ModNext.Mod.ToString().Replace('_', ' ') + " starts\nin ";
        }
        ModifierText.text += Mathf.Clamp(Mathf.FloorToInt(ModifierManager.RemainingTime), 0, int.MaxValue).ToString("000");
    }
}
