﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace MoreMountains.Tools {
    [RequireComponent(typeof(Text))]
    public class BetterFPSCounter : MonoBehaviour {

        Text Text;
        int framesPrev = 0;

        protected virtual void Start() {
            Text = GetComponent<Text>();
            StartCoroutine(UpdateText());
        }

        IEnumerator UpdateText() {
            while (enabled && this != null) {
                int framesNow = Time.renderedFrameCount;
                Text.text = 4*(framesNow - framesPrev) + " FPS";
                framesPrev = framesNow;
                yield return new WaitForSecondsRealtime(0.25f);
            }
        }
    }
}
