﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScalingCharacterHandleWeapon : CharacterHandleWeapon {

    public override void ChangeWeapon(Weapon newWeapon) {
        if (CurrentWeapon == null) {
            base.ChangeWeapon(newWeapon);
            return;
        }

        base.ChangeWeapon(newWeapon);

        Vector3 scaleWeapon = CurrentWeapon.transform.localScale;
        Vector3 scalePlayerInit = GetComponent<CharacterUpdateStats>().initialSize;
        CurrentWeapon.transform.localScale = new Vector3(
            scaleWeapon.x * (transform.localScale.x / scalePlayerInit.x),
            scaleWeapon.y * (transform.localScale.y / scalePlayerInit.y),
            scaleWeapon.z * (transform.localScale.z / scalePlayerInit.z)
        );
    }

}
