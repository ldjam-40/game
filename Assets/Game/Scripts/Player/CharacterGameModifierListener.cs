﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Character))]
[RequireComponent(typeof(CorgiController))]
[RequireComponent(typeof(CharacterGravity))]
public class CharacterGameModifierListener : MonoBehaviour, MMEventListener<GameModifierManager.ModifierEvent> {

    private Character Character;
    private CorgiController Controller;
    private CharacterGravity Gravity;

    void Start() {
        Character = GetComponent<Character>();
        Controller = GetComponent<CorgiController>();
        Gravity = GetComponent<CharacterGravity>();
        
        MMEventManager.AddListener(this);
    }

    void Update() {
		
	}

    public void OnMMEvent(GameModifierManager.ModifierEvent mod) {
        bool enabled = mod.Enabled;
        switch (mod.Mod) {
            case GameModifierManager.Modifier.Moon:
                const float factor = 2f / 5f;
                if (enabled) {
                    Controller.DefaultParameters.Gravity *= factor;
                } else {
                    Controller.DefaultParameters.Gravity /= factor;
                }
                break;
        }
    }

}
