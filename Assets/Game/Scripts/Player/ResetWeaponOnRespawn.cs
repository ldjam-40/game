﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(CharacterHandleWeapon))]
public class ResetWeaponOnRespawn : CharacterAbility {

    CharacterHandleWeapon HandleWeapon;

    void Start() {
        HandleWeapon = GetComponent<CharacterHandleWeapon>();
    }

    protected override void OnRespawn() {
        base.OnRespawn();

        HandleWeapon.ChangeWeapon(HandleWeapon.InitialWeapon);
    }

}
