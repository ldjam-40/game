﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Health))]
public class SpawnInvincibility : CharacterAbility {

    public float Duration = 1f;

    private Health Health;

    private Renderer[] Renderers;

    void Start() {
        Health = GetComponent<Health>();

        OnRespawn();
    }

    protected override void OnRespawn() {
        base.OnRespawn();

        Health.Invulnerable = true;

        Renderers = GetComponentsInChildren<Renderer>();
        StartCoroutine(Flash());
        StartCoroutine(Timeout());
    }

    IEnumerator Flash() {
        while (Health.Invulnerable) {
            Opacity(0.1f);
            yield return null;
            yield return null;
            Opacity(0.9f);
            yield return null;
            yield return null;
        }
        Opacity(1);
    }

    IEnumerator Timeout() {
        yield return new WaitForSeconds(Duration);
        Health.Invulnerable = false;
    }

    void Opacity(float a) {
        for (int i = 0; i < Renderers.Length; i++) {
            Renderer renderer = Renderers[i];
            if (renderer == null)
                continue;
            if (!renderer.sharedMaterial.name.EndsWith(" - SpawnInvincibility clone")) {
                renderer.sharedMaterial = Instantiate(renderer.sharedMaterial);
                renderer.sharedMaterial.name += " - SpawnInvincibility clone";
            }
            if (!renderer.sharedMaterial.HasProperty("_Color"))
                continue;
            Color c = renderer.sharedMaterial.color;
            renderer.sharedMaterial.color = new Color(c.r, c.g, c.b, a);
        }
    }

}
