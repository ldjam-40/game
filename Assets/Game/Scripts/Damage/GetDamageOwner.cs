﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface GetDamageOwner {
	GameObject GetDamageOwner();
}
